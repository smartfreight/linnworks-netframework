﻿using System.IO;
using System.Drawing;
using PdfPrintingNet;
//using SmartFreight.Engine.Core;
using System;

namespace LinnworksIntegration
{
    public sealed class Converter
    {
        PdfPrint pdfPrint;
        byte[] _doc;

        public Converter(byte[] doc)
        {
            pdfPrint = new PdfPrint("Interactive Freight Systems pty Ltd", "g/4JFMjn6Kvpfox/442Cn9tqpK+IQcLKQh/pN91yLair3tMeY0PIxCJ6T1CPJ8T6xXNbnOtSmKswKty3CMgfae1rOim90Zd/IAuvva0P0oRZPc08O4Q7L86onidFFdiD3Wnn+gzQ2PHbvcmRIYrA/Q==");
            _doc = doc;
        }

        public int TotalPages()
        {
            return pdfPrint.GetNumberOfPages(_doc);
        }

        public byte[] GetPageAsImage(int page)
        {
            var bitmap = pdfPrint.GetBitmapFromPdfPage(_doc, page);
            MemoryStream ms = new MemoryStream();
            bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            return ms.ToArray();
        }

        public SizeF GetPageSize(int page)
        {
            return pdfPrint.GetPdfPageSize(_doc, page);
        }


    }
}
