using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;

namespace LinnworksIntegration
{
    public static class ExtPropertyMap
    {
        [FunctionName("ExtPropertyMap")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            var oep = new List<ExtendedPropertyMapping>() {
                            new ExtendedPropertyMapping
                            {
                                PropertyTitle = "Sender EORI",
                                PropertyName = "Sender EORI",
                                PropertyDescription = "Sender EORI",
                                PropertyType = "ORDER"
                            },
                            new ExtendedPropertyMapping
                            {
                                PropertyTitle = "Receiver EORI",
                                PropertyName = "Receiver EORI",
                                PropertyDescription = "Receiver EORI",
                                PropertyType = "ORDER"
                            },
                            new ExtendedPropertyMapping
                            {
                                PropertyTitle = "Sender VAT",
                                PropertyName = "Sender VAT",
                                PropertyDescription = "Sender VAT",
                                PropertyType = "ORDER"
                            }
                        };
            var respobj = new
            {
                Items = oep.ToArray(),
                IsError = false,
                ErrorMessage = "null"
            };
            var jsonToReturn = JsonConvert.SerializeObject(respobj);
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(jsonToReturn, Encoding.UTF8, "application/json")
            };
        }
    }
}
