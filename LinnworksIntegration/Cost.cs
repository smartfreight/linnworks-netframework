using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace LinnworksIntegration
{
    public static class Cost
    {
        [FunctionName("Cost")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            //var jsonToReturn = "{\"QuoteItems\": [{\"ServiceName\": \"ACME GLOBAL TPT | RUN1\",\"ServiceCode\": \"RUN1\",\"ServiceId\": \"176e4426-2d38-4825-b4bb-ca798c151a36\",\"ServiceTag\": \"RUN1\",\"CollectionDate\": \"/Date(1613433600)/\",\"EstimatedDeliveryDate\": \"/Date(1613476800)/\",\"Cost\": 1.75,\"Tax\": 0.35,\"TotalCost\": 2.10,\"Currency\": \"GBP\",\"PropertyItem\": [],\"Options\": []},{\"ServiceName\": \"ACME GLOBAL TPT | RUN2\",\"ServiceCode\": \"RUN2\",\"ServiceId\": \"176e4426-2d38-4825-b4bb-ca798c151a36\",\"ServiceTag\": \"RUN2\",\"CollectionDate\": \"/Date(1613433600)/\",\"EstimatedDeliveryDate\": \"/Date(1613476800)/\",\"Cost\": 1.95,\"Tax\": 0.39,\"TotalCost\": 2.34,\"Currency\": \"GBP\",\"PropertyItem\": [],\"Options\": []},{\"ServiceName\": \"ACME GLOBAL TPT | RUN3\",\"ServiceCode\": \"RUN3\",\"ServiceId\": \"176e4426-2d38-4825-b4bb-ca798c151a36\",\"ServiceTag\": \"RUN3\",\"CollectionDate\": \"/Date(1613433600)/\",\"EstimatedDeliveryDate\": \"/Date(1613476800)/\",\"Cost\": 2.15,\"Tax\": 0.43,\"TotalCost\": 2.58,\"Currency\": \"GBP\",\"PropertyItem\": [],\"Options\": []},{\"ServiceName\": \"DPD LOCAL | 1030AM\",\"ServiceCode\": \"1030AM\",\"ServiceId\": \"176e4426-2d38-4825-b4bb-ca798c151a36\",\"ServiceTag\": \"1030AM\",\"CollectionDate\": \"/Date(1613433600)/\",\"EstimatedDeliveryDate\": \"/Date(1613557800)/\",\"Cost\": 5.50,\"Tax\": 1.10,\"TotalCost\": 6.60,\"Currency\": \"GBP\",\"PropertyItem\": [],\"Options\": []}],\"IsError\": false,\"ErrorMessage\": \"null\"}";

            var myObj = new Object();
            try
            {
                //var content = await new StreamReader(req.Body).ReadToEndAsync();
                //OrderDetails order = JsonConvert.DeserializeObject<OrderDetails>(content);
                OrderDetails order = await req.Content.ReadAsAsync<OrderDetails>();
                log.Info("Cost Request received at " + DateTime.Now.ToString() + " with body: " + JsonConvert.SerializeObject(order));


                CloudStorageAccount storageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=sf1linnworks;AccountKey=u48Jg6eB2YdhhLAQKgjLN6U9H2vX4KZz0WmVtwYeZRXekkZoFJiJsvlvtH0WqEnpkITtwXatonTpzrSDOusGUA==;EndpointSuffix=core.windows.net");
                CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
                CloudTable table = tableClient.GetTableReference("users");
                TableOperation operation = TableOperation.Retrieve<UserEntity>("SF1", order.AuthorizationToken);
                var result = await table.ExecuteAsync(operation);
                var user = (UserEntity)result.Result;


                table = tableClient.GetTableReference("services");
                TableContinuationToken token = null;
                var services = await table.ExecuteQuerySegmentedAsync(new TableQuery<ServiceEntity>(), token);

                SFOnline.SFOv1Client sv = new SFOnline.SFOv1Client();
                var list = new List<CostResponse>();

                foreach (var oservice in services.Results)
                {
                    var service = (ServiceEntity)oservice;
                    string xml = ConvertOrderToXml(order, service.servicename);
                    var resp = await sv.CostComparisonAsync(user.EntityId, user.ApiKey, "", xml);
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(resp);
                    XmlNodeList nodes = doc.SelectNodes("/CostComparisonResults/ResultSet/Result");
                    //foreach (XmlNode node in nodes)
                    //{
                    if (nodes.Count > 0)
                    {
                        var quoteresp = new CostResponse();
                        quoteresp.ServiceName = service.servicename;
                        quoteresp.ServiceCode = "[" + service.servicename + "]";
                        quoteresp.ServiceId = service.RowKey;
                        quoteresp.ServiceTag = service.servicename;
                        long coldateunix = (long)(DateTime.Today.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
                        quoteresp.CollectionDate = "/Date(" + coldateunix + ")/";
                        var date = DateTime.Parse(nodes[0]["EstDelivery"].InnerText);
                        long deldateunix = (long)(date.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
                        quoteresp.EstimatedDeliveryDate = "/Date(" + deldateunix + ")/";
                        quoteresp.Cost = Convert.ToDecimal(nodes[0]["TotalCost"].InnerText);
                        quoteresp.Tax = Convert.ToDecimal(nodes[0]["GSTAmount"].InnerText);
                        quoteresp.TotalCost = Convert.ToDecimal(nodes[0]["TotalCostIncTax"].InnerText);
                        quoteresp.Currency = order.OrderCurrency;
                        quoteresp.PropertyItem = new string[] { };
                        quoteresp.Options = new string[] { };
                        list.Add(quoteresp);
                    }                    
                    //}
                }

                myObj = new
                {
                    QuoteItems = list.ToArray(),
                    IsError = false,
                    ErrorMessage = "null"
                };
            }
            catch (Exception e)
            {
                myObj = new
                {
                    IsError = true,
                    ErrorMessage = e.Message
                };
            }
            var jsonToReturn = JsonConvert.SerializeObject(myObj);
            log.Info("Cost Request response at " + DateTime.Now.ToString() + " with body: " + jsonToReturn);
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(jsonToReturn, Encoding.UTF8, "application/json")
            };
        }

        private static string ConvertOrderToXml(OrderDetails order, string service)
        {
            StringBuilder xml = new StringBuilder();
            xml.Append("<connote>");
            xml.Append("<recaddr>");
            xml.Append("<add1>" + order.AddressLine1 + "</add1>");
            xml.Append("<add2>" + order.AddressLine2 + "</add2>");
            xml.Append("<add3>" + order.Town + "</add3>");
            xml.Append("<add4>" + order.Region + "</add4>");
            xml.Append("<add5>" + order.PostalCode + "</add5>");
            var code = CountryCodes.Codes[order.CountryCode] != null ? CountryCodes.Codes[order.CountryCode] : "";
            if (code == "")
            {
                throw new Exception("Invalid country code for receiver");
            }
            xml.Append("<add6>" + code + "</add6>");
            xml.Append("</recaddr>");
            xml.Append("<carriername>[" + service + "]</carriername>");
            xml.Append("<chargeto>S</chargeto>");
            foreach (var item in order.Packages)
            {
                xml.Append("<freightlinedetails>");
                xml.Append("<ref>" + order.OrderReference + "</ref>");
                xml.Append("<desc>" + item.PackageFormat + "</desc>");
                xml.Append("<amt>" + item.items.Length + "</amt>");
                xml.Append("<wgt>" + item.PackageWeight + "</wgt>");
                xml.Append("<len>" + item.PackageDepth + "</len>");
                xml.Append("<hgt>" + item.PackageHeight + "</hgt>");
                xml.Append("<wdt>" + item.PackageWidth + "</wdt>");
                xml.Append("</freightlinedetails>");
            }
            xml.Append("</connote>");
            return xml.ToString();
        }
    }
}
