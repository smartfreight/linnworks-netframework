using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;

namespace LinnworksIntegration
{
    public static class Delete
    {
        [FunctionName("Delete")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            var respobj = new Object();
            try
            {
                //var content = await new StreamReader(req.Body).ReadToEndAsync();
                //UserConfig config = JsonConvert.DeserializeObject<UserConfig>(content);
                UserConfig config = await req.Content.ReadAsAsync<UserConfig>();

                UserEntity user = new UserEntity();
                user.PartitionKey = "SF1";
                user.ETag = "*";
                user.RowKey = config.AuthorizationToken;

                CloudStorageAccount storageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=sf1linnworks;AccountKey=u48Jg6eB2YdhhLAQKgjLN6U9H2vX4KZz0WmVtwYeZRXekkZoFJiJsvlvtH0WqEnpkITtwXatonTpzrSDOusGUA==;EndpointSuffix=core.windows.net");
                CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
                CloudTable table = tableClient.GetTableReference("users");
                TableOperation operation = TableOperation.Delete(user);
                await table.ExecuteAsync(operation);

                respobj = new
                {
                    IsError = false,
                    ErrorMessage = "null"
                };
            }
            catch (Exception e)
            {
                respobj = new
                {
                    IsError = true,
                    ErrorMessage = e.Message
                };
            }
            var jsonToReturn = JsonConvert.SerializeObject(respobj);

            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(jsonToReturn, Encoding.UTF8, "application/json")
            };
        }
    }
}
