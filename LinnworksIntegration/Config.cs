using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LinnworksIntegration
{
    public static class Config
    {
        [FunctionName("Config")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            var respobj = new ConfigResponse();
            var errobj = new Object();
            bool haserror = false;
            try
            {
                //var content = await new StreamReader(req.Body).ReadToEndAsync();
                //UserConfig config = JsonConvert.DeserializeObject<UserConfig>(content);
                UserConfig config = await req.Content.ReadAsAsync<UserConfig>();

                CloudStorageAccount storageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=sf1linnworks;AccountKey=u48Jg6eB2YdhhLAQKgjLN6U9H2vX4KZz0WmVtwYeZRXekkZoFJiJsvlvtH0WqEnpkITtwXatonTpzrSDOusGUA==;EndpointSuffix=core.windows.net");
                CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
                CloudTable table = tableClient.GetTableReference("users");
                TableOperation operation = TableOperation.Retrieve<UserEntity>("SF1", config.AuthorizationToken);
                var result = await table.ExecuteAsync(operation);
                var user = (UserEntity)result.Result;

                respobj.ConfigStatus = "Credentials";
                respobj.IsError = false;
                respobj.ErrorMessage = "null";
                respobj.IsConfigActive = user.IsActive;
                respobj.ConfigStage = new Stage();
                respobj.ConfigStage.WizardStepDescription = "Please enter your SmartFreight Entity ID and API Key. If you don't have those contact our European Channel Manager Natasha Jones via +44 (0) 333 996 2216 or Natasha.Jones@wisetechglobal.com. For international shipments you will also need to provide your EORI number and VAT number. If you do not have those you can leave those empty for now.";
                respobj.ConfigStage.WizardStepTitle = "SmartFreight Credentials";

                List<ConfigItem> items = new List<ConfigItem>();
                ConfigItem item = new ConfigItem();
                item.ConfigItemId = "ENTITYID";
                item.Description = "SmartFreight Entity ID";
                item.GroupName = "Credentials";
                item.MustBeSpecified = true;
                item.Name = "Entity ID";
                item.ReadOnly = false;
                item.SelectedValue = user.IsActive ? user.EntityId : "";
                item.SortOrder = 1;
                item.ValueType = 0;
                items.Add(item);
                item = new ConfigItem();
                item.ConfigItemId = "APIKEY";
                item.Description = "SmartFreight API Key";
                item.GroupName = "Credentials";
                item.MustBeSpecified = true;
                item.Name = "API Key";
                item.ReadOnly = false;
                item.SelectedValue = user.IsActive ? user.ApiKey : "";
                item.SortOrder = 1;
                item.ValueType = 0;
                items.Add(item);
                item = new ConfigItem();
                item.ConfigItemId = "SENDEREORI";
                item.Description = "Sender's EORI Number";
                item.GroupName = "Credentials";
                item.MustBeSpecified = false;
                item.Name = "Sender EORI";
                item.ReadOnly = false;
                item.SelectedValue = user.IsActive ? user.SendEORI : "";
                item.SortOrder = 1;
                item.ValueType = 0;
                items.Add(item);
                item = new ConfigItem();
                item.ConfigItemId = "SENDERVAT";
                item.Description = "Sender's VAT number";
                item.GroupName = "Credentials";
                item.MustBeSpecified = false;
                item.Name = "Sender VAT";
                item.ReadOnly = false;
                item.SelectedValue = user.IsActive ? user.SendVAT : "";
                item.SortOrder = 1;
                item.ValueType = 0;
                items.Add(item);

                respobj.ConfigStage.ConfigItems = items.ToArray();
            }
            catch (Exception e)
            {
                haserror = true;
                errobj = new
                {
                    IsError = true,
                    ErrorMessage = e.Message
                };
            }
            var jsonToReturn = JsonConvert.SerializeObject(!haserror ? respobj : errobj);
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(jsonToReturn, Encoding.UTF8, "application/json")
            };
        }
    }
}

