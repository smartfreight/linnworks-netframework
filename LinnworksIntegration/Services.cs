using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;

namespace LinnworksIntegration
{
    public static class Services
    {
        [FunctionName("Services")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            var respobj = new AvailableServices();
            var errobj = new Object();
            bool haserror = false;
            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=sf1linnworks;AccountKey=u48Jg6eB2YdhhLAQKgjLN6U9H2vX4KZz0WmVtwYeZRXekkZoFJiJsvlvtH0WqEnpkITtwXatonTpzrSDOusGUA==;EndpointSuffix=core.windows.net");
                CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
                CloudTable table = tableClient.GetTableReference("services");
                string filter = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, "SF1");
                TableContinuationToken token = null;
                var result = await table.ExecuteQuerySegmentedAsync(new TableQuery<ServiceEntity>().Where(filter), token);

                respobj.IsError = false;
                respobj.ErrorMessage = "null";
                List<CourierService> courierservices = new List<CourierService>();

                foreach (ServiceEntity service in result.Results)
                {
                    var courierservice = new CourierService();
                    courierservice.ServiceCode = "[" + service.servicename + "]";
                    courierservice.ServiceGroup = "Services";
                    courierservice.ServiceName = service.servicename;
                    courierservice.ServiceTag = service.servicename;
                    courierservice.ServiceUniqueId = service.RowKey;
                    var serviceprop = new ServiceProperty();
                    serviceprop.PropertyName = "LABELTAG";
                    serviceprop.PropertyValue = service.servicename;
                    courierservice.ServiceProperty = (new List<ServiceProperty>() { serviceprop }).ToArray();


                    var configItems = new List<ConfigItem>() {
                            new ConfigItem() {
                                ConfigItemId="",
                                Description="",
                                GroupName = "No configuration required",
                                MustBeSpecified = false,
                                Name="No Configuration",
                                ReadOnly = false,
                                SortOrder=1,
                                SelectedValue = "true",
                                ValueType = 3
                            }
                        };

                    courierservice.ConfigItems = configItems.ToArray();
                    courierservices.Add(courierservice);
                }
                respobj.Services = courierservices.ToArray();
            }
            catch (Exception e)
            {
                haserror = true;
                errobj = new
                {
                    IsError = true,
                    ErrorMessage = e.Message
                };
            }
            var jsonToReturn = JsonConvert.SerializeObject(!haserror ? respobj : errobj);
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(jsonToReturn, Encoding.UTF8, "application/json")
            };
        }
    }
}
