using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;

namespace LinnworksIntegration
{
    public static class UpdateConfig
    {
        [FunctionName("UpdateConfig")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            var respobj = new Object();
            try
            {
                //var content = await new StreamReader(req.Body).ReadToEndAsync();
                //ConfigUpdate config = JsonConvert.DeserializeObject<ConfigUpdate>(content);
                ConfigUpdate config = await req.Content.ReadAsAsync<ConfigUpdate>();

                UserEntity user = new UserEntity();
                user.AuthToken = config.AuthorizationToken;
                user.IsActive = true;
                user.ETag = "*";
                foreach (var conf in config.ConfigItems)
                {
                    if (conf.ConfigItemId == "ENTITYID")
                    {
                        user.EntityId = conf.SelectedValue;
                    }
                    else if (conf.ConfigItemId == "APIKEY")
                    {
                        user.ApiKey = conf.SelectedValue;
                    }
                    else if (conf.ConfigItemId == "SENDEREORI")
                    {
                        user.SendEORI = conf.SelectedValue;
                    }
                    else if (conf.ConfigItemId == "SENDERVAT")
                    {
                        user.SendVAT = conf.SelectedValue;
                    }
                }
                user.PartitionKey = "SF1";
                user.RowKey = config.AuthorizationToken;

                CloudStorageAccount storageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=sf1linnworks;AccountKey=u48Jg6eB2YdhhLAQKgjLN6U9H2vX4KZz0WmVtwYeZRXekkZoFJiJsvlvtH0WqEnpkITtwXatonTpzrSDOusGUA==;EndpointSuffix=core.windows.net");
                CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
                CloudTable table = tableClient.GetTableReference("users");
                TableOperation operation = TableOperation.Merge(user);
                await table.ExecuteAsync(operation);

                respobj = new
                {
                    IsError = false,
                    ErrorMessage = "null"
                };
            }
            catch (Exception e)
            {
                respobj = new
                {
                    IsError = true,
                    ErrorMessage = e.Message
                };
            }
            var jsonToReturn = JsonConvert.SerializeObject(respobj);
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(jsonToReturn, Encoding.UTF8, "application/json")
            };
        }
    }
}
