﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;

namespace LinnworksIntegration
{
    public class OrderDetails
    {
        public string AuthorizationToken { get; set; }
        public string Name { get; set; }
        public string CompanyName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string Town { get; set; }
        public string Region { get; set; }
        public string CountryCode { get; set; }
        public string PostalCode { get; set; }
        public string DeliveryNote { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int OrderId { get; set; }
        public string OrderReference { get; set; }
        public string OrderCurrency { get; set; }
        public decimal OrderValue { get; set; }
        public decimal PostageCharges { get; set; }
        public string ServiceId { get; set; }
        public Package[] Packages { get; set; }
        public OrderExtendedProperties[] OrderExtendedProperties { get; set; }
        public ServiceConfigItem[] SaveConfigItems { get; set; }
    }

    public class Package
    {
        public int SequenceNumber { get; set; }
        public decimal PackageWeight { get; set; }
        public decimal PackageWidth { get; set; }
        public decimal PackageHeight { get; set; }
        public decimal PackageDepth { get; set; }
        public string PackageFormat { get; set; }
        public Item[] items { get; set; }
    }

    public class OrderExtendedProperties
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class ServiceConfigItem
    {
        public string ConfigItemId { get; set; }
        public string SelectedValue { get; set; }
    }

    public class Item
    {
        public string ItemName { get; set; }
        public string ProductCode { get; set; }
        public int Quantity { get; set; }
        public decimal UnitValue { get; set; }
        public decimal UnitWeight { get; set; }
        public decimal Height { get; set; }
        public decimal Width { get; set; }
        public decimal Length { get; set; }
        public ExtendedProperty[] ExtendedProperties { get; set; }
    }

    public class ExtendedProperty
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class Cancellation
    {
        public string AuthorizationToken { get; set; }
        public string OrderReference { get; set; }
    }

    public class ImportResponse
    {
        public string LeadTrackingNumber { get; set; }
        public decimal Cost { get; set; }
        public string Currency { get; set; }
        public ImportResponsePackage[] Package { get; set; }
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class ImportResponsePackage
    {
        public string SequenceNumber { get; set; }
        public string TrackingNumber { get; set; }
        public string PNGLabelDataBase64 { get; set; }
        public Array PDFBytesDocumentationBase64 { get; set; }
        public decimal LabelWidth { get; set; }
        public decimal LabelHeight { get; set; }
    }

    public class CostResponse
    {
        public string ServiceName { get; set; }
        public string ServiceCode { get; set; }
        public string ServiceId { get; set; }
        public string ServiceTag { get; set; }
        public string CollectionDate { get; set; }
        public string EstimatedDeliveryDate { get; set; }
        public decimal Cost { get; set; }
        public decimal Tax { get; set; }
        public decimal TotalCost { get; set; }
        public string Currency { get; set; }
        public Array PropertyItem { get; set; }
        public Array Options { get; set; }
    }

    public static class CountryCodes
    {
        public static Dictionary<string, string> Codes = new Dictionary<string, string>() {
   { "AF", "AFGHANISTAN" },    // Afghanistan
   { "AL", "ALBANIA" },    // Albania
   { "AE", "UNITED ARAB EMIRATES" },    // U.A.E.
   { "AR", "ARGENTINA" },    // Argentina
   { "AM", "ARMENIA" },    // Armenia
   { "AU", "AUSTRALIA" },    // Australia
   { "AT", "AUSTRIA" },    // Austria
   { "AZ", "AZERBAIJAN" },    // Azerbaijan
   { "BE", "BELGIUM" },    // Belgium
   { "BD", "BANGLADESH" },    // Bangladesh
   { "BG", "BULGARIA" },    // Bulgaria
   { "BH", "BAHRAIN" },    // Bahrain
   { "BA", "BOSNIA"},    // Bosnia and Herzegovina
   { "BY", "BELARUS" },    // Belarus
   { "BZ", "BELIZE" },    // Belize
   { "BO", "BOLIVIA" },    // Bolivia
   { "BR", "BRAZIL" },    // Brazil
   { "BN", "BRUNEI DARUSSALAM" },    // Brunei Darussalam
   { "CA", "CANADA" },    // Canada
   { "CH", "SWITZERLAND" },    // Switzerland
   { "CL", "CHILE" },    // Chile
   { "CN", "CHINA" },    // People's Republic of China
   { "CO", "COLUMBIA" },    // Colombia
   { "CR", "COSTA RICA" },    // Costa Rica
   { "CZ", "CZECH REPUBLIC" },    // Czech Republic
   { "DE", "GERMANY" },    // Germany
   { "DK", "DENMARK" },    // Denmark
   { "DO", "DOMINICAN REPUBLIC" },    // Dominican Republic
   { "DZ", "ALGERIA" },    // Algeria
   { "EC", "ECUADOR" },    // Ecuador
   { "EG", "EGYPT" },    // Egypt
   { "ES", "SPAIN" },    // Spain
   { "EE", "ESTONIA" },    // Estonia
   { "ET", "ETHIOPIA" },    // Ethiopia
   { "FI", "FINLAND" },    // Finland
   { "FR", "FRANCE" },    // France
   { "FO", "FAROE ISLANDS" },    // Faroe Islands
   { "GB", "UNITED KINGDOM" },    // United Kingdom
   { "GE", "GEORGIA" },    // Georgia
   { "GR", "GREECE" },    // Greece
   { "GL", "GREENLAND" },    // Greenland
   { "GT", "GUATEMALA" },    // Guatemala
   { "HK", "HONG KONG" },    // Hong Kong S.A.R.
   { "HN", "HONDURAS" },    // Honduras
   { "HR", "CROATIA" },    // Croatia
   { "HU", "HUNGARY" },    // Hungary
   { "ID", "INDONESIA" },    // Indonesia
   { "IN", "INDIA" },    // India
   { "IE", "IRELAND" },    // Ireland
   {"IM", "ISLE OF MAN" },
   { "IR", "IRAN" },    // Iran
   { "IQ", "IRAQ" },    // Iraq
   { "IS", "ICELAND" },    // Iceland
   { "IL", "ISREAL" },    // Israel
   { "IT", "ITALY" },    // Italy
   { "JM", "JAMAICA" },    // Jamaica
   { "JO", "JORDAN" },    // Jordan
   { "JP", "JAPAN" },    // Japan
   { "KZ", "KAZAKHSTAN" },    // Kazakhstan
   { "KE", "KENYA" },    // Kenya
   { "KG", "KYRGYZSTAN" },    // Kyrgyzstan
   { "KH", "CAMBODIA" },    // Cambodia
   { "KR", "KOREA" },    // Korea
   { "KW", "KUWAIT" },    // Kuwait
   { "LA", "LAO PDR" },    // Lao P.D.R.
   { "LB", "LEBANON" },    // Lebanon
   { "LY", "LIBYA" },    // Libya
   { "LI", "LIECHTENSTEIN" },    // Liechtenstein
   { "LK", "SRI LANKA" },    // Sri Lanka
   { "LT", "LITHUANIA" },    // Lithuania
   { "LU", "LUXEMBOURG" },    // Luxembourg
   { "LV", "LATVIA" },    // Latvia
   { "MO", "MACAO" },    // Macao S.A.R.
   { "MA", "MOROCCO" },    // Morocco
   { "MC", "MONACO" },    // Principality of Monaco
   { "MV", "MALDIVES" },    // Maldives
   { "MX", "MEXICO" },    // Mexico
   { "MK", "MACEDONIA" },    // Macedonia (FYROM)
   { "MT", "MALTA" },    // Malta
   { "ME", "MONTENEGRO" },    // Montenegro
   { "MN", "MONGOLIA" },    // Mongolia
   { "MY", "MALAYSIA" },    // Malaysia
   { "NG", "NIGERIA" },    // Nigeria
   { "NI", "NICARAGUA" },    // Nicaragua
   { "NL", "NETHERLANDS" },    // Netherlands
   { "NO", "NORWAY" },    // Norway
   { "NP", "NEPAL" },    // Nepal
   { "NZ", "NEW ZEALAND" },    // New Zealand
   { "OM", "OMAN" },    // Oman
   { "PK", "PAKISTAN" },    // Islamic Republic of Pakistan
   { "PA", "PANAMA" },    // Panama
   { "PE", "PERU" },    // Peru
   { "PH", "PHILIPPINES" },    // Republic of the Philippines
   { "PL", "POLAND" },    // Poland
   { "PR", "PUERTO RICO" },    // Puerto Rico
   { "PT", "PORTUGAL" },    // Portugal
   { "PY", "PARAGUAY" },    // Paraguay
   { "QA", "QATAR" },    // Qatar
   { "RO", "ROMANIA" },    // Romania
   { "RU", "RUSSIA" },    // Russia
   { "RW", "RWANDA" },    // Rwanda
   { "SA", "SAUDI ARABIA" },    // Saudi Arabia
   { "CS", "SERBIA" },    // Serbia and Montenegro (Former)
   { "SN", "SENEGAL" },    // Senegal
   { "SG", "SINGAPORE" },    // Singapore
   { "SV", "EL SALVADOR" },    // El Salvador
   { "RS", "SERBIA" },    // Serbia
   { "SK", "SLOVAKIA" },    // Slovakia
   { "SI", "SLOVENIA" },    // Slovenia
   { "SE", "SWEDEN" },    // Sweden
   { "SY", "SYRIA" },    // Syria
   { "TJ", "TAJIKISTAN" },    // Tajikistan
   { "TH", "THAILAND" },    // Thailand
   { "TM", "TURKMENISTAN" },    // Turkmenistan
   { "TT", "TRINIDAD" },    // Trinidad and Tobago
   { "TN", "TUNISIA" },    // Tunisia
   { "TR", "TURKEY" },    // Turkey
   { "TW", "TAIWAN" },    // Taiwan
   { "UA", "UKRAINE" },    // Ukraine
   { "UY", "URUGUAY" },    // Uruguay
   { "US", "UNITED STATES" },    // United States
   { "UZ", "UZBEKISTAN" },    // Uzbekistan
   { "VE", "VENEZUELA" },    // Bolivarian Republic of Venezuela
   { "VN", "VIETNAM" },    // Vietnam
   { "XA", "NORTHERN IRELAND" },
   { "YE", "YEMEN" },    // Yemen
   { "ZA", "SOUTH AFRICA" },    // South Africa
   { "ZW", "ZIMBABWE" },    // Zimbabwe
};
    }

    public class NewUser
    {
        public string Email { get; set; }
        public Guid LinnworksUniqueIdentifier { get; set; }
        public string AccountName { get; set; }
    }

    public class UserConfig
    {
        public string AuthorizationToken { get; set; }
    }

    public class UserEntity : TableEntity
    {
        public UserEntity(string authtoken)
        {
            this.PartitionKey = "SF1";
            this.RowKey = authtoken;
        }
        public UserEntity() { }
        public string LinnworksId { get; set; }
        public string AuthToken { get; set; }
        public string Email { get; set; }
        public string AccName { get; set; }
        public bool IsActive { get; set; }
        public string EntityId { get; set; }
        public string ApiKey { get; set; }
        public string SendEORI { get; set; }
        public string SendVAT { get; set; }
    }

    public class LogEntity : TableEntity
    {
        public LogEntity(string entityid)
        {
            this.PartitionKey = "SF1";
            this.RowKey = entityid;
        }
        public LogEntity() { }
        public string Response { get; set; }
    }

    public class ServiceEntity : TableEntity
    {
        public ServiceEntity(string guid)
        {
            this.PartitionKey = "SF1";
            this.RowKey = guid;
        }
        public ServiceEntity() { }
        public string servicename { get; set; }
    }

    public class ConfigResponse
    {
        public bool IsConfigActive { get; set; }
        public string ConfigStatus { get; set; }
        public Stage ConfigStage { get; set; }
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class Stage
    {
        public string WizardStepDescription { get; set; }
        public string WizardStepTitle { get; set; }
        public ConfigItem[] ConfigItems { get; set; }
    }

    public class ConfigItem
    {
        public string ConfigItemId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string GroupName { get; set; }
        public int SortOrder { get; set; }
        public string SelectedValue { get; set; }
        public string RegExValidation { get; set; }
        public string RegExError { get; set; }
        public bool MustBeSpecified { get; set; }
        public bool ReadOnly { get; set; }
        public ConfigItemListItem[] ListValues { get; set; }
        public int ValueType { get; set; }
    }

    public class ConfigItemListItem
    {
        public string Display { get; set; }
        public string Value { get; set; }
    }

    public class ConfigUpdate
    {
        public string AuthorizationToken { get; set; }
        public string ConfigStatus { get; set; }
        public ConfigItemValue[] ConfigItems { get; set; }
    }

    public class ConfigItemValue
    {
        public string ConfigItemId { get; set; }
        public string SelectedValue { get; set; }
    }

    public class AvailableServices
    {
        public CourierService[] Services { get; set; }
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class CourierService
    {
        public string ServiceName { get; set; }
        public string ServiceCode { get; set; }
        public string ServiceTag { get; set; }
        public string ServiceGroup { get; set; }
        public string ServiceUniqueId { get; set; }
        public ConfigItem[] ConfigItems { get; set; }
        public ServiceProperty[] ServiceProperty { get; set; }
    }

    public class ServiceProperty
    {
        public string PropertyName { get; set; }
        public string PropertyValue { get; set; }
    }

    public class ExtendedPropertyMapping
    {
        public string PropertyTitle { get; set; }
        public string PropertyName { get; set; }
        public string PropertyDescription { get; set; }
        public string PropertyType { get; set; }
    }
}