using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace LinnworksIntegration
{
    public static class Import
    {
        [FunctionName("Import")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            var respobj = new ImportResponse();
            var errobj = new Object();
            bool haserror = false;
            try
            {
                OrderDetails order = await req.Content.ReadAsAsync<OrderDetails>();
                //var content = await new StreamReader(req.).ReadToEndAsync();
                //OrderDetails order = JsonConvert.DeserializeObject<OrderDetails>(content);       

                ValidateImportFields(order, out List<string> errorList);

                log.Info("Import Request received at " + DateTime.Now.ToString() + " with body: " + JsonConvert.SerializeObject(order));

                CloudStorageAccount storageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=sf1linnworks;AccountKey=u48Jg6eB2YdhhLAQKgjLN6U9H2vX4KZz0WmVtwYeZRXekkZoFJiJsvlvtH0WqEnpkITtwXatonTpzrSDOusGUA==;EndpointSuffix=core.windows.net");
                CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
                CloudTable table = tableClient.GetTableReference("services");
                TableOperation operation = TableOperation.Retrieve<ServiceEntity>("SF1", order.ServiceId);
                var result = await table.ExecuteAsync(operation);
                if (result.Result == null && string.IsNullOrEmpty(result.Etag))
                {
                    throw new Exception("[Service] Please ensure that a valid SmartFreight service has been selected for the shipment");
                }
                var servicename = ((ServiceEntity)result.Result).servicename;
                
                //string xml = ConvertOrderToXml(out List<string> sequencenumbers);

                table = tableClient.GetTableReference("users");
                operation = TableOperation.Retrieve<UserEntity>("SF1", order.AuthorizationToken);
                result = await table.ExecuteAsync(operation);
                if (result.Result == null && string.IsNullOrEmpty(result.Etag))
                {
                    throw new Exception("[Authorization Token] Please ensure that you are registered with SmartFreight and the plugin has been correctly configured with your credentials");
                }
                var user = (UserEntity)result.Result;

                if (order.CountryCode != "GB" && string.IsNullOrEmpty(user.SendEORI))
                {
                    errorList.Add("[Sender EORI] Please ensure that you have provided your EORI number in this shipping integration's configuration screen. This is a requirement for international shipments");
                }

                if (order.CountryCode != "GB" && string.IsNullOrEmpty(user.SendVAT))
                {
                    errorList.Add("[Sender VAT] Please ensure that you have provided your VAT number in this shipping integration's configuration screen. This is a requirement for international shipments");
                }                

                if (errorList.Count > 0)
                {
                    throw new Exception($"Please correct the following error(s) before creating a shipment: {string.Join("; ", errorList.ToArray())}");
                }

                var additionalrefs = new Dictionary<string, string>();
                if (!string.IsNullOrEmpty(user.SendEORI))
                {
                    additionalrefs.Add("send_eori", user.SendEORI);
                }
                if (!string.IsNullOrEmpty(user.SendVAT))
                {
                    additionalrefs.Add("send_vat", user.SendVAT);
                }

                string xml = ConvertOrderToXml(order, out List<string> sequencenumbers, servicename, additionalrefs);

                SFOnline.SFOv1Client sv = new SFOnline.SFOv1Client();
                var resp = await sv.ImportAsync(user.EntityId, user.ApiKey, "", xml.ToString());
                //var resp = await sv.ImportAsync("6CM", "nu02N8SJ9ZkXruvPPyfcPihs2odIsns_Ro", "", xml.ToString());

                //get label from resp and then other details
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(resp);

                if (resp.Contains("HasErrors=\"True\""))
                {
                    throw new Exception(resp);
                }

                XmlNode node = doc.DocumentElement;
                var label = node["labels"];
                int totalitems = Convert.ToInt32(node["totitems"].InnerText);
                byte[] b = Convert.FromBase64String(node["printjob"].InnerText);
                var converter = new Converter(b);

                int numberofpages = converter.TotalPages();
                //int numberofpages = Splitter.NumberOfPages(b);
                int splitmultiples = (int)(numberofpages / totalitems);
                //List<byte[]> l = Splitter.SplitPDF(b, splitmultiples);
                XmlNodeList trackingnumbers = doc.SelectNodes("/Connote/labels");
                XmlNodeList freightlines = doc.SelectNodes("/Connote/freightlinedetails");

                respobj.LeadTrackingNumber = label["labelno_tracking"].InnerText;
                respobj.Cost = Convert.ToDecimal(node["grandtotrate"].InnerText);
                //respobj.Currency = node["_currency"].InnerText;
                respobj.Currency = order.OrderCurrency;
                List<ImportResponsePackage> packages = new List<ImportResponsePackage>();
                int index = 0;
                int freightlineindex = 0;
                foreach (XmlNode freightline in freightlines)
                {
                    int noofitems = Convert.ToInt32(freightline["amt"].InnerText);
                    for (int i = 0; i < noofitems; i++)
                    {
                        var package = new ImportResponsePackage();
                        package.SequenceNumber = freightline["sku"] != null ? freightline["sku"].InnerText : sequencenumbers[freightlineindex];
                        package.TrackingNumber = trackingnumbers.Item(index)["labelno_tracking"].InnerText;
                        var lbytes = converter.GetPageAsImage(index + 1);
                        package.PNGLabelDataBase64 = Convert.ToBase64String(lbytes);
                        package.PDFBytesDocumentationBase64 = new string[] { };
                        var size = converter.GetPageSize(index + 1);
                        //var size = Splitter.LabelSize(l[index]);
                        //package.LabelWidth = size["Width"];
                        //package.LabelHeight = size["Height"];
                        package.LabelWidth = Convert.ToDecimal(size.Width);
                        package.LabelHeight = Convert.ToDecimal(size.Height);
                        packages.Add(package);
                        index++;
                    }
                    freightlineindex++;
                }
                respobj.Package = packages.ToArray();
                respobj.IsError = false;
                respobj.ErrorMessage = null;
            }
            catch (Exception e)
            {
                haserror = true;
                errobj = new
                {
                    IsError = true,
                    ErrorMessage = e.Message
                };
            }
            var jsonToReturn = JsonConvert.SerializeObject(!haserror ? respobj : errobj);
            log.Info("Import Request response at " + DateTime.Now.ToString() + " with body: " + jsonToReturn);
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(jsonToReturn, Encoding.UTF8, "application/json")
            };
        }

        private static void ValidateImportFields(OrderDetails order, out List<string> errorList)
        {
            errorList = new List<string>();
            if (string.IsNullOrEmpty(order.OrderId.ToString()) && order.OrderId > 0)
            {
                errorList.Add("[Order ID] Please ensure that the order ID is populated and provided");
            }
            if (string.IsNullOrEmpty(order.Name))
            {
                errorList.Add("[Name] Please ensure that the receiver's name is populated");
            }
            if (string.IsNullOrEmpty(order.Phone))
            {
                errorList.Add("[Phone Number] Please ensure that the receiver's phone number is populated");
            }
            if (string.IsNullOrEmpty(order.Email))
            {
                errorList.Add("[Email Address] Please ensure that the receiver's email address is populated");
            }
            if (string.IsNullOrEmpty(order.AddressLine1))
            {
                errorList.Add("[Address Line 1] Please ensure that the receiver's address line 1 is populated");
            }
            if (string.IsNullOrEmpty(order.Town))
            {
                errorList.Add("[Town] Please ensure that the receiver's town is populated");
            }
            if (string.IsNullOrEmpty(order.PostalCode))
            {
                errorList.Add("[Postal Code] Please ensure that the receiver's postcode is populated");
            }
            if (string.IsNullOrEmpty(order.CountryCode))
            {
                errorList.Add("[Country] Please ensure that the receiver's country is populated");
            }
            foreach (var item in order.Packages)
            {
                if (string.IsNullOrEmpty(item.PackageFormat) || item.PackageFormat.ToLower() != "carton")
                {
                    errorList.Add("[Package Type] Please ensure that the package type has been set to carton");
                }
                if (item.PackageWeight <= 0)
                {
                    errorList.Add("[Package Weight] Please ensure that the package weight has been provided and is greater than 0");
                }
                if (item.items.Length <= 0)
                {
                    errorList.Add("[Packages] Please ensure that the package count has been provided and is greater than 0");
                }
                if (item.PackageDepth <= 0 || item.PackageHeight <= 0 || item.PackageWidth <= 0)
                {
                    errorList.Add("[Package Dimensions] Please ensure that the package dimensions have been provided and are greater than 0");
                }
            }
            if (string.IsNullOrEmpty(order.OrderCurrency))
            {
                errorList.Add("[Currency] Please ensure that the order currency has been provided");
            }
            if (order.CountryCode != "GB")
            {
                bool hasreceori = false;
                foreach (var prop in order.OrderExtendedProperties)
                {
                    if (prop.Name.ToLower() == "receiver eori")
                    {
                        hasreceori = true;
                    }
                }
                if (!hasreceori)
                {
                    errorList.Add("[Receiver EORI] Please ensure that you have provided an Order Extended Property called Receiver EORI with the receivers EORI number for an international shipment. If you don't have the the receiver's EORI number just enter a value of 0");
                }
            }
        }

        private static string ConvertOrderToXml(OrderDetails order, out List<string> sequencenumbers, string servicename, Dictionary<string,string> additionalrefs)
        {
            StringBuilder xml = new StringBuilder();
            xml.Append("<connote>");
            xml.Append("<condate>" + DateTime.Today.ToString("dd/MM/yyyy") + "</condate>");
            xml.Append("<recaccno>" + order.OrderId + "</recaccno>");
            xml.Append("<recname>" + (!string.IsNullOrEmpty(order.CompanyName) ? order.CompanyName : order.Name) + "</recname>");
            xml.Append("<recph>" + order.Phone + "</recph>");
            xml.Append("<reccontact>" + order.Name + "</reccontact>");
            xml.Append("<recemail>" + order.Email + "</recemail>");
            xml.Append("<recaddr>");
            xml.Append("<add1>" + order.AddressLine1 + "</add1>");
            xml.Append("<add2>" + order.AddressLine2 + "</add2>");
            xml.Append("<add3>" + order.Town + "</add3>");
            xml.Append("<add4>" + order.Region + "</add4>");
            xml.Append("<add5>" + order.PostalCode + "</add5>");
            //var code = CountryCodes.Codes[order.CountryCode] != null ? CountryCodes.Codes[order.CountryCode] : "";
            //if (code == "")
            //{
            //    throw new Exception("Invalid country code for receiver");
            //}
            var country = CountryCodes.Codes[order.CountryCode];
            xml.Append("<add6>" + country + "</add6>");
            xml.Append("</recaddr>");
            xml.Append("<carriername>[" + servicename + "]</carriername>");
            xml.Append("<spins>");
            xml.Append("<Sp1>" + order.DeliveryNote + "</Sp1>");
            xml.Append("</spins>");
            xml.Append("<countrymanufacture>" + country + "</countrymanufacture>");
            xml.Append("<goodscurrency>" + order.OrderCurrency + "</goodscurrency>");
            sequencenumbers = new List<string>();
            foreach (var package in order.Packages)
            {
                string goods = "";
                string hscode = "";
                int quantity = 0;
                decimal value = 0;
                foreach (var item in package.items)
                {
                    if (goods == "" && !string.IsNullOrEmpty(item.ItemName))
                    {
                        goods = item.ItemName;
                    }
                    if (hscode == "" && !string.IsNullOrEmpty(item.ProductCode))
                    {
                        hscode = item.ProductCode;
                    }
                    quantity += item.Quantity;
                    value += item.UnitValue;
                }

                xml.Append("<freightlinedetails>");
                xml.Append("<ref>" + order.OrderReference + "</ref>");
                xml.Append("<sku>" + package.SequenceNumber + "</sku>");
                sequencenumbers.Add(package.SequenceNumber.ToString());
                xml.Append("<desc>" + package.PackageFormat + "</desc>");
                xml.Append("<amt>1</amt>");
                xml.Append("<wgt>" + (package.PackageWeight / 1000) + "</wgt>");
                xml.Append("<len>" + package.PackageDepth + "</len>");
                xml.Append("<hgt>" + package.PackageHeight + "</hgt>");
                xml.Append("<wdt>" + package.PackageWidth + "</wdt>");
                xml.Append("<goods>" + goods + "</goods>");
                xml.Append("<hscode>" + hscode + "</hscode>");
                xml.Append("<pieces>" + quantity + "</pieces>");
                xml.Append("<val>" + value + "</val>");
                xml.Append("</freightlinedetails>");
            }
            //if (order.CountryCode != "GB")
            //{
            foreach (var prop in order.OrderExtendedProperties)
            {
                if (prop.Name.ToLower() == "receiver eori")
                {
                    xml.Append("<additionalreferences>");
                    xml.Append("<predefinedtype>rec_eori</predefinedtype>");
                    xml.Append("<referenceno>" + prop.Value + "</referenceno>");
                    xml.Append("</additionalreferences>");
                }
                else
                {
                    xml.Append("<additionalreferences>");
                    xml.Append("<referencetype>" + prop.Name + "</referencetype>");
                    xml.Append("<referenceno>" + prop.Value + "</referenceno>");
                    xml.Append("</additionalreferences>");
                }
            }
            if (additionalrefs.Count > 0)
            {
                foreach(var addref in additionalrefs)
                {
                    xml.Append("<additionalreferences>");
                    xml.Append("<predefinedtype>" + addref.Key + "</predefinedtype>");
                    xml.Append("<referenceno>" + addref.Value + "</referenceno>");
                    xml.Append("</additionalreferences>");
                }
            }
            //}
            xml.Append("<labeltype>B64PDF</labeltype>");
            xml.Append("<adhocrec>true</adhocrec>");
            xml.Append("</connote>");

            return xml.ToString();
        }

        private static string ConvertOrderToXml(out List<string> sequencenumbers)
        {
            StringBuilder xml = new StringBuilder();
            xml.Append("<connote>");
            xml.Append("<condate>" + DateTime.Today.AddDays(1).ToString("dd/MM/yyyy") + "</condate>");
            //xml.Append("<recaccno>" + order.Name + "</recaccno>");
            xml.Append("<recaccno>" + "TEST" + "</recaccno>");
            //xml.Append("<recname>" + order.CompanyName + "</recname>");
            xml.Append("<recname>" + "Test Receiver" + " </recname>");
            //xml.Append("<recph>" + order.Phone + "</recph>");
            xml.Append("<recph>02 8888 8888</recph>");
            //xml.Append("<reccontact>" + order.Name + "</reccontact>");
            xml.Append("<reccontact>Warehouse Manager</reccontact>");
            //xml.Append("<recemail>" + order.Email + "</recemail>");
            xml.Append("<recemail>test@test.com</recemail>");
            xml.Append("<recaddr>");
            //xml.Append("<add1>" + order.AddressLine1 + "</add1>");
            xml.Append("<add1>Level 10</add1>");
            //xml.Append("<add2>" + order.AddressLine2 + "</add2>");
            xml.Append("<add2>100 George Street</add2>");
            //xml.Append("<add3>" + order.Town + "</add3>");
            xml.Append("<add3>LONDON</add3>");
            //xml.Append("<add4>" + order.Region + "</add4>");
            xml.Append("<add4>LONDON</add4>");
            //xml.Append("<add5>" + order.PostalCode + "</add5>");
            xml.Append("<add5>SW6 1HS</add5>");
            //var code = CountryCodes.Codes[order.CountryCode] != null ? CountryCodes.Codes[order.CountryCode] : "";
            //if (code == "")
            //{
            //    throw new Exception("Invalid country code for receiver");
            //}
            //xml.Append("<add6>" + code + "</add6>");
            xml.Append("<add6>United Kingdom</add6>");
            xml.Append("</recaddr>");
            //xml.Append("<carriername>[" + servicename + "]</carriername>");
            xml.Append("<carriername>[Automatic]</carriername>");
            //xml.Append("<spins>");
            //xml.Append("<Sp1>" + order.DeliveryNote + "</Sp1>");
            //xml.Append("</spins>");
            xml.Append("<spins>");
            xml.Append("<Sp1>Special Instruction</Sp1>");
            xml.Append("</spins>");
            sequencenumbers = new List<string>();
            //foreach (var item in order.Packages)
            //{
            xml.Append("<freightlinedetails>");
            //xml.Append("<ref>" + order.OrderReference + "</ref>");
            //xml.Append("<sku>" + item.SequenceNumber + "</sku>");
            //sequencenumbers.Add(item.SequenceNumber.ToString());
            xml.Append("<sku>SEQUENCE NUMBER</sku>");
            xml.Append("<ref>ref11</ref>");
            //xml.Append("<desc>" + item.PackageFormat + "</desc>");
            xml.Append("<desc>CARTON</desc>");
            //xml.Append("<amt>" + item.items.Length + "</amt>");
            xml.Append("<amt>1</amt>");
            //xml.Append("<wgt>" + item.PackageWeight + "</wgt>");
            xml.Append("<wgt>2</wgt>");
            //xml.Append("<len>" + item.PackageDepth + "</len>");
            xml.Append("<len>27</len>");
            //xml.Append("<hgt>" + item.PackageHeight + "</hgt>");
            xml.Append("<hgt>6</hgt>");
            //xml.Append("<wdt>" + item.PackageWidth + "</wdt>");
            xml.Append("<wdt>17</wdt>");
            xml.Append("</freightlinedetails>");

            xml.Append("<freightlinedetails>");
            //xml.Append("<ref>" + order.OrderReference + "</ref>");
            //xml.Append("<desc>" + item.PackageFormat + "</desc>");
            xml.Append("<sku>SEQUENCE NUMBER</sku>");
            xml.Append("<ref>ref11</ref>");
            xml.Append("<desc>CARTON</desc>");
            //xml.Append("<amt>" + item.items.Length + "</amt>");
            xml.Append("<amt>3</amt>");
            //xml.Append("<wgt>" + item.PackageWeight + "</wgt>");
            xml.Append("<wgt>5</wgt>");
            //xml.Append("<len>" + item.PackageDepth + "</len>");
            xml.Append("<len>5</len>");
            //xml.Append("<hgt>" + item.PackageHeight + "</hgt>");
            xml.Append("<hgt>5</hgt>");
            //xml.Append("<wdt>" + item.PackageWidth + "</wdt>");
            xml.Append("<wdt>5</wdt>");
            xml.Append("</freightlinedetails>");
            //}
            xml.Append("<labeltype>B64PDF</labeltype>");
            xml.Append("<adhocrec>true</adhocrec>");
            xml.Append("</connote>");

            return xml.ToString();
        }
    }
}
