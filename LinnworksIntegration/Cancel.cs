using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LinnworksIntegration
{
    public static class Cancel
    {
        [FunctionName("Cancel")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            var respobj = new Object();
            try
            {
                //var content = await new StreamReader(req.Body).ReadToEndAsync();
                //Cancellation cancelation = JsonConvert.DeserializeObject<Cancellation>(content);
                Cancellation cancelation = await req.Content.ReadAsAsync<Cancellation>();

                CloudStorageAccount storageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=sf1linnworks;AccountKey=u48Jg6eB2YdhhLAQKgjLN6U9H2vX4KZz0WmVtwYeZRXekkZoFJiJsvlvtH0WqEnpkITtwXatonTpzrSDOusGUA==;EndpointSuffix=core.windows.net");
                CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
                CloudTable table = tableClient.GetTableReference("users");
                TableOperation operation = TableOperation.Retrieve<UserEntity>("SF1", cancelation.AuthorizationToken);
                var result = await table.ExecuteAsync(operation);
                var user = (UserEntity)result.Result;

                SFOnline.SFOv1Client sv = new SFOnline.SFOv1Client();
                var resp = await sv.FindConAsync(user.EntityId, user.ApiKey, "recaccno", cancelation.OrderReference, "");
                //get conid from response
                var conid = resp[0];
                //delete con
                var deleteresp = await sv.DeleteConAsync(user.EntityId, user.ApiKey, conid);

                respobj = new
                {
                    IsError = false,
                    ErrorMessage = "null"

                };
            }
            catch (Exception e)
            {
                respobj = new
                {
                    IsError = true,
                    ErrorMessage = e.Message
                };
            }
            var jsonToReturn = JsonConvert.SerializeObject(respobj);
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(jsonToReturn, Encoding.UTF8, "application/json")
            };
        }
    }
}