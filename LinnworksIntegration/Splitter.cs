﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Text;

namespace LinnworksIntegration
{
    public sealed class Splitter
    {
        public static List<byte[]> SplitPDF(byte[] pdf, int splitmultiples = 1)
        {
            /*splitmulitples - if set to 1 - each entry in list<byte[]> will be 1 page
             *                 if set to 2 - each entry in list<byte[]> will be 2 pages etc..
             */

            try
            {
                List<byte[]> l = new List<byte[]>();
                PdfReader pdfreader = new PdfReader(pdf);
                int noofpages = pdfreader.NumberOfPages;
                for (int i = 1; i <= noofpages; i += splitmultiples)
                {
                    //create document
                    using (Document doc = new Document())
                    using (System.IO.MemoryStream m = new System.IO.MemoryStream())
                    using (PdfCopy pdfcopy = new PdfCopy(doc, m))
                    {
                        int endpage = (i + splitmultiples - 1) <= noofpages ? (i + splitmultiples - 1) : noofpages;
                        doc.Open();
                        for (int q = i; q <= endpage; q++)
                        {
                            pdfcopy.AddPage(pdfcopy.GetImportedPage(pdfreader, q));
                        }
                        doc.Close();
                        l.Add(m.ToArray());
                        //string b64 = Convert.ToBase64String(m.ToArray());
                    }
                }
                return l;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static int NumberOfPages(byte[] pdf)
        {
            try
            {
                return new PdfReader(pdf).NumberOfPages;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static Dictionary<string, decimal> LabelSize(byte[] pdf)
        {
            try
            {
                Rectangle lbl = new PdfReader(pdf).GetPageSize(1);
                var dic = new Dictionary<string, decimal>();
                dic.Add("Width", (decimal)lbl.Width);
                dic.Add("Height", (decimal)lbl.Height);
                return dic;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
